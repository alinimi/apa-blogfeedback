
setwd("~/FIB/APA/blogFeedback")
data <- read.csv(file = "datos.train.csv", header = TRUE, row.names = 1)
data.test <- read.csv(file = "datos.test.csv", header = TRUE, row.names = 1)
data <- as.data.frame(data)
data.test <- as.data.frame(data.test)



###escalar datos train
data.scaled <- scale(data)
data.scaled <- as.data.frame(data.scaled)
data.scaled$t <- data[,which(names(data)=='t')]
names(data.scaled)[names(data.scaled)=='data$t'] <- 't'



###escalar datos test
data.test.scaled <- scale(data.test)
data.test.scaled <- as.data.frame(data.test.scaled)
data.test.scaled$t <- data.test[,which(names(data.test)=='t')]
names(data.test.scaled)[names(data.test.scaled)=='data$t'] <- 't'
data.test.scaled[,which(colSums(data.test)==0)] <- rep(0,nrow(data.test.scaled))





require(nnet)




N <- nrow(data)

(lambdes <- 10^seq(-3,0,by=0.5))
K <- 10


errorLam <- rep(0,length(lambdes))
errors <- rep(0,10)
bestLambdes <- rep(0,10)


###K-fold cv
folds <- sample(rep(1:K, length=N), N, replace=FALSE)
for (k in seq(1,K))
{
  training <- data.scaled[folds!=k,]
  validation.x <- data.scaled[folds==k,-which(names(data)=='t')]
  validation.t <- data.scaled[folds==k,which(names(data)=='t')]
  
  # Compute the NRMSE for each lambda
  for (lam in seq(1,length(lambdes)))
  {
    nnet.fit <- nnet(t ~ ., data=training, size=30,  MaxNWts = 3000,
                     linout =TRUE, decay =lam, maxit = 1000)
    nnet.predict <- predict(nnet.fit, newdata = validation.x)
    error <- sqrt(sum((nnet.predict - data.test.scaled$t)^2)/((nrow(data.scaled)-1)*var(data.scaled$t)))
    errorLam[lam] <- errorLam[lam] + error 
  }
}


errorLam <- errorLam/(K)
  
###elegir la lambda con menor NRMSE medio
indexBest <- which(errorLam == min(errorLam))
bestLambda <- lambdes[indexBest]


###hacer el modelo con todos los datos
nnet.model <- nnet(t ~ ., data=data.scaled, size=30,  MaxNWts = 3000,
                 linout =TRUE, decay =bestLambda, maxit = 1500)






nnet.predict <- predict(nnet.model, newdata = data.test.scaled)

###Calcular el NRMSE de las predicciones de los datos de test
(errorsTest <- sqrt(sum((nnet.predict - data.test.scaled$t)^2)/((nrow(data.test.scaled)-1)*var(data.test.scaled$t))))


plot(data.test.scaled$t, nnet.predict,
     main="Neural network predictions vs actual",
     xlab="Actual")

