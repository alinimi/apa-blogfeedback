#########################################################################
################RIDGE REGRESSION

###leer los datos
setwd("~/FIB/APA/blogFeedback")
data <- read.csv(file = "datos.train.csv", header = TRUE, row.names = 1)
data.test <- read.csv(file = "datos.test.csv", header = TRUE, row.names = 1)
data <- as.data.frame(data)
data.test <- as.data.frame(data.test)


library(MASS)

###obtener el modelo de regresión lineal
linreg <- glm (t ~ ., data = data, family = "gaussian")
linreg <- step(linreg)


###empezamos con una búsqueda logarítmica de lambda
lambdes <- 10^seq(-3,5,by=0.1)
10^seq(-3,5,by=0.1)

select( lm.ridge(linreg, lambda = lambdes) )


###mejoramos la precisión
lambdes <- seq(0.5,0.7, by = 0.01)
select( lm.ridge(linreg, lambda = lambdes) )

lambdes <- seq(0.57,0.59, by = 0.001)
select(lm.ridge(linreg, lambda = lambdes) )

###generamos el modelo con la mejor lambda
ridgereg <- lm.ridge(linreg, lambda = 0.584)


N <- (ncol(data)-1)


###diferentes funciones de error
mse = function(x,y) { mean((x-y)^2)}
msese = function(x,y) { sqrt(var((x-y)^2)) }

ridgereg$scales
names(ridgereg$coef)

###escalamos las variables relevantes y predecimos el número de comentarios de los datos de test
ytpredg = scale(data.test[,which(names(data.test)%in%names(ridgereg$coef))],center = F, scale = ridgereg$scales)%*% ridgereg$coef + mean(data.test$t)

mse(ytpredg, data.test$t)
msese(ytpredg,data.test$t)


###utilizamos el error NRMSE para comparar los diferentes modelos
(NRMSE.VA.regul <- sqrt(sum((data.test$t - ytpredg)^2)/((nrow(data.test))*var(data.test$t))))



data.test.x <- data.test[,-(which(names(data)=='t'))]
data.test.x <- as.data.frame(data.test.x)

summary((ytpredg - data.test$t)^2)


###plot de los resultados obtenidos con el modelo vs los resultados del dataset 
plot(data.test$t, ytpredg,
     main="Ridge regression predictions vs actual",
     xlab="Actual")

